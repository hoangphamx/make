#include <iostream>
#include "test.h"
#include "Math.h"



int main(int argc, char const *argv[])
{
    test();

    Lib::Math math {3};
    auto id = math.getId();
    std::cout << "id = " << id << std::endl;

    return 0;
}
