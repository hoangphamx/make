#include <iostream>
#include "Math.h"

namespace Lib
{
    Math::Math(int id)
        :mId(id)
    {
    }

    int Math::getId() const noexcept
    {
        return mId;
    }
}

// int lib1(int x)
// {
//     std::cout << "lib1" << std::endl;
//     return x;
// }