#ifndef CMAKE_LIB_MATH_H
#define CMAKE_LIB_MATH_H

namespace Lib
{
    class Math
    {
    private:
        int mId;
    public:
        explicit Math(int id);
        int getId() const noexcept;
    };
}

#endif