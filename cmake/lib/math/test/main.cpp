#include <iostream>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../Math.h"


TEST(lib1, test1)
{
    Lib::Math math {3};
    auto id = math.getId();

    EXPECT_EQ(id, 4) << "FAILED - lib1.test1 - EXPECT_EQ(id, 4) - id = " << id;
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    return ret;
}